/**
 * FILENAME :       acid.js
 *
 * DESCRIPTION :	Auto identify presence of js,html,php coupling in files. If code for the
 * 			incorrect file extension is found it will warn and highlight the line.
 *
 *
 * USAGE:
 *			node acid.js <PATH/TO/TARGET>
 *
 *
 * LICENSE:
 *       Copyright Network Silence. 2018.  All rights reserved.
 *
 * Show don't Sell License Version 1
 *   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
 *
 *
 * AUTHOR :   Network Silence        START DATE :    10 Feb 2020
 *
 */

const file_system = require("fs");
const { resolve } = require("path");
const { readdir } = require("fs").promises;
const arguments = require("yargs")
  .usage("Usage $0 <path>")
  .command("path", "Path to file or directory")
  .demandCommand(1, "Required File or Path")
  .help("h")
  .alias("h", "help")
  .epilog("Network Silence copyright 2020").argv;

const languages = {
  ".js": new RegExp("<script[sS]*?>[sS]*?</script>", "g"),
  ".php": new RegExp("<?php(.+?)?>", "g"),
  ".html": new RegExp(
    "</?w+((s+w+(s*=s*(?:\".*?\"|'.*?'|[^'\">s]+))?)+s*|s*)/?>",
    "g"
  ),
  ".css": new RegExp('styles*=s*"([^"]*)"', "g")
};
const file_type = {
  file: 1,
  directory: 2,
  error: -1
};

/**
 * Search files for occurences of coupled code I.e code not associated to the file extension
 *
 * @async
 * @param {String} language_name - Current file's file extension
 * @param {Regex} language_regex - Regex to match against
 * @param {String} file_name - Name of file we are analyzing
 */
async function search_files(language_name, language_regex, file_name) {
  let matched = [];
  let data = "";
  let read_stream = file_system.createReadStream(file_name, "utf8");
  read_stream
    .on("data", function(chunk) {
      data += chunk;
    })
    .on("end", function() {
      matched = data.match(language_regex);
      if (matched !== null) {
        for (let match of matched) {
          console.log(
            language_name.substr(1) +
              " in file:  " +
              file_name +
              " match: " +
              match
          );
        }
      } else {
        console.log("No coupling");
      }
    });
}

/**
 * Identify if a path points to a file or a directory
 *
 * @param {String} path - Path to validate
 * @return {Int} Return enum int associated with file type.
 */
function validate_path(path) {
  if (file_system.statSync(path).isDirectory()) {
    return file_type.directory;
  } else if (file_system.statSync(path).isFile()) {
    return file_type.file;
  } else {
    return file_type.error;
  }
}

/**
 * Get file extension of the passed in file.
 *
 * @param {String} file_name - File name to pull extension from
 * @return {String} return file extension including . ex ".jpg"
 */
function get_extension(file_name) {
  let last_dot = file_name.lastIndexOf(".");
  return last_dot < 0 ? "" : file_name.substr(last_dot);
}

/**
 *  Loop through all files of a given directory. And sub directories
 *
 * @async Subdirectories are recursively searched using yield as a call back to
 * 	keep the async look running.
 * @param {String} dir - Directory which we want to look through
 * @return {String} File name currently present in the dir
 */
async function* get_files(dir) {
  const dirents = await readdir(dir, { withFileTypes: true });
  for (const dirent of dirents) {
    const res = resolve(dir, dirent.name);
    if (dirent.isDirectory()) {
      yield* get_files(res);
    } else {
      yield res;
    }
  }
}

/**
 * Identify what checks the extensions needs to validate against.
 *
 * @async
 * @param {String} file_name - Name of file whose content we are analyzing.
 * @param {String} extension - Extension of the file we are analyzing.
 */
async function check_file(file_name, extension) {
  for (let [key, value] of Object.entries(languages)) {
    if (languages[key] !== languages[extension]) {
      search_files(key, value, file_name).catch(console.error);
    }
  }
}

/**
 * Main function. Start of code
 *
 * @async
 * @param {Yargs array} argv - Array containing the user passed parameters
 */
async function main(argv) {
  let file_name = argv._[0];
  let extension = "";
  switch (validate_path(file_name)) {
    case file_type.directory:
      (async () => {
        for await (const f of get_files(file_name)) {
          extension = get_extension(f);
          check_file(f, extension).catch(console.error);
        }
      })();
      break;
    case file_type.file:
      extension = get_extension(file_name);
      await check_file(file_name, extension).catch(console.error);
      break;
    default:
      console.error("Unsupported type");
  }
}
main(arguments);
