# Acid

The following was made as a small POC utility to flag bad practices of merging php,js and html together.
**NOTE** The following uses REGEX for matching and should not be relied on for accurate matching.

### Installation

1. Install `nodejs` and `npm` on your machine.
2. Clone this repo and navigate to the newly created directory.
3. Type in `npm install`

### Usage

Run on a file or directory with the following `node acid.js <FilePath/DirPath>`

```
Usage acid.js <path>

Commands:
  acid.js path  Path to file or directory

Options:
  --version   Show version number                                      [boolean]
  -h, --help  Show help                                                [boolean]

Network Silence copyright 2020

```

### License

```
       Copyright Network Silence. 2020.  All rights reserved.

 Show don't Sell License Version 1
   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
```
